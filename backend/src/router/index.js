import {createRouter, createWebHistory} from "vue-router";
import Dashboard from "@/components/NavBar.vue";
import Login from "@/views/Login.vue";
import RequestPassword from "@/views/RequestPassword.vue";
import SetNewPassword from "@/views/SetNewPassword.vue";
import EsdTableOverview from "@/views/EsdTableOverview.vue";

const routes = [
    {
        path: '/dashboard',
        name: 'dashboard',
        component: Dashboard
    },
    {
        path: '/login',
        name: 'login',
        component: Login
    },
    {
        path: '/request-new-password',
        name:'Request new password',
        component: RequestPassword
    },
    {
        path: '/reset-password/token',
        name: 'Set new password',
        component: SetNewPassword
    },
    {
        path: '/overview',
        name: 'Overview',
        component: EsdTableOverview
    }
]

const router = createRouter({
    history: createWebHistory(),
    routes
})

export default router
